# 51610001

DHU 講義 5161001 数学探訪の最終レポートリポジトリ.

| ディレクトリ名/ファイル | 概要 |
| -- | -- |
| 5161src/ | レポート内で示したソースコード |
| assets/ | レポート内で示した図などの生成スクリプト等 |
| report.md | markdown 形式のレポート本文 |
| Rakefile | レポート生成のためのファイル. 内部で pandoc と plantuml を利用している. |

レポート内で示した各コードは, bitbucket-pipelines によってテストしている.
レポート内の日本語文章は, 校正ツール textlint を利用している.
レポートのビルドは`npm run build`で実行する.
