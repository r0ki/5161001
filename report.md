---
title: 私の数学探訪
author: A15DC077 五味拓樹
date: \today
fontsize: 11pt
header-includes:
    - \usepackage{luatexja}
      \usepackage{color}
      \usepackage{amssymb}
      \usepackage{tikz}
      \usetikzlibrary{shapes,arrows}
      \hypersetup{colorlinks=false}
      \usepackage{setspace}
      \usepackage{amsthm}
abstract: |
    本レポートでは, 講義「数学探訪」の最終レポート「私の数学探訪」を示す. 
    私の今までのプログラム開発経験のうち, 特に数学的特性を活かしたものであったと思ういくつかの項目について, 簡単に取り上げる.
...

\newpage

# 本レポート内のソースコードについて

本レポート内では, 実例を示すためにいくつかソースコードを掲載している.
それらは,

* [Haskell 2010](https://www.haskell.org/onlinereport/haskell2010/)

に準じて記述される.
これは, 文書の都合上, 比較的コードを短くできる傾向にあるプログラミング言語として選択されただけであり, 特別な理由はない.
なお, 文書中に示されたソースコードは, 継続的インテグレーションツールによってユニットテストをパスしている.
また, より実用的な参考実装として,

* [ISO/IEC 14882:2017(E) – Programming Language C++](https://www.iso.org/standard/68564.html)[^1]

に準じて記述されたコードへの参照を示している.

[^1]: 厳密には, C++1z のドラフト規格 [N4659](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/n4659.pdf) を参照している.

\newpage

# De Bruijn Sequence と Number of Training Zero

De Bruijn Sequence は, オランダ人の数学者 Nicolaas de Bruijn に因んで命名された系列で, 特定の長さのすべての組み合わせを含む系列である. 
次数 $n$ の $k$ 種類に関する De Bruijn Sequence $B\left(k, n\right)$ は, 長さ $n$ で表現可能なすべての部分列によって構成される.
次元数 $2$ (すなわちバイナリ) の De Bruijn Sequence は $$B\left(2, n\right)$$ であり, $n$ ビットの固有な部分系列から成る $2^n$ ビット長の系列である.
例えば, $B\left(2, 3\right)$ は $00011101_{(2)}$ であり $n$ に対する有向グラフが下図[^2]のように示される.

![n = 2 の De Bruijn diagraphs.](./assets/deb_graph1.png){height=300}

この系列から $3$ ビットずつ取る, または図 1 の有向グラフから $B(2, 3)$ を再構築していくと, 
次の表で示す部分系列を構成することがわかる.

\newpage

| $B(2, 3)$ | $10$ 進値 |
| -- | -- |
| $\overbrace{000}^{sub\ seq}11101_{(2)}$ | $0_{(10)}$ |
| $0\overbrace{001}^{sub\ seq}1101_{(2)}$ | $1_{(10)}$ |
| $00\overbrace{011}^{sub\ seq}101_{(2)}$ | $3_{(10)}$ |
| $000\overbrace{111}^{sub\ seq}01_{(2)}$ | $7_{(10)}$ | 
| $0001\overbrace{110}^{sub\ seq}1_{(2)}$ | $6_{(10)}$ |
| ${00011\overbrace{101}^{sub\ seq}}_{(2)}$ | $5_{(10)}$ | 
| ${000111\overbrace{01\underbrace{0}_{cir}}^{sub\ seq}}_{(2)}$ | $2_{(10)}$ |
| ${0001110\overbrace{1\underbrace{00}_{cir}}^{sub\ seq}}_{(2)}$ | $4_{(10)}$ |

最後の $2$ つの部分系列は $00011101_{(2)}$ から $3$ ビットずつとって構成できないが, 系列の初めへ循環していると考えることで, これが成り立つ.

De Bruijn Sequence は, いくつかのコンピュータアルゴリズムで応用でき, 例えば, Number of Training Zero を求める問題もよく知られた応用例の 1 つである. 
Number of Training Zero は, ビット列を一番右端から見て, 一番初めに立っているビット位置を $n$ としたとき, $n-1$ を求めることを言う. これは ntz と呼ばれる.
以降, $m=n-1$, $x$ を $8$ ビットの値, $x_i\ \ \left(\left\{i \in \mathbb{Z}\mid 0 < i < 9\right\}\right)$ を右から見た値
$x$ の $i$ 番目のビット値, Number of Training Zero を ntz とする.

例えば, $x=192_{(10)}$ は

$$x=1\overbrace{\overbrace{1}^{x_7}\underbrace{\overbrace{0}^{x_6}00000}_{m}}^{n}$$

であり, $m=6$ が解である.

ntz をプログラムで解こうとしたとき, 例えば次のような実装がよく知られる.

\newpage

```haskell
module Main where

import Control.Monad (void)
import Data.Bits ((.&.), shiftR)
import Data.Word (Word8)
import Data.Tuple.Extra (first, second, dupe)
import Test.HUnit (runTestText, putTextToHandle, Test (TestList), (~:), (~?=))
import System.IO (stderr)

-- | pop counting 8 bit
popcnt8 :: Word8 -> Word8
popcnt8 = let fol m s = uncurry (+) . first (.&. m) . 
                second ((.&. m) . (`shiftR` s)) . dupe in
                    flip (foldr id) [
                        fol (0x0f :: Word8) 4, 
                        fol (0x33 :: Word8) 2, 
                        fol (0x55 :: Word8) 1]
 
 -- | ntz 8 bit version 1
ntz81 :: Word8 -> Word8
ntz81 = uncurry id . 
    first (bool (popcnt8 . pred . uncurry (.&.) . second negate . dupe) 
        (\_ -> 8 :: Word8) . (== 0)) . dupe

main :: IO ()
main = void . runTestText (putTextToHandle stderr False) $
    TestList ["ntz8 192: " ~: ntz81 (192 :: Word8) ~?= 6]
```

各関数について簡単に述べる. まず`popcnt8`は立っているビット数を得る関数である. 各ビットの桁をそのビットが立っている個数と捉え, 
$55_{(16)} = 1010101_{(2)}, 33_{(16)} = 110011_{(2)}, 0f_{(16)} = 00001111_{(2)}$ のマスクとシフト演算を利用して, 
その個数を $2$ 桁ごと, $4$ 桁ごと $\cdots$ とまとめていき, 最終的に全体の立っているビット数を得る.
`ntz81`は, ntz を計算する関数である. $2$ の補数表現を活かし, 右端の立っているビットのみを残した後 $-1$ の加算を行うことで,
$m$ 個のビット(この場合 $x_1$ から $x_6$)を全て立てる. これを`popcnt8`に渡すことで, ntz が得られる.
これはよく最適化された手法であると言えるが, De Bruijn Sequence を利用すると, さらに少ない命令数で ntz が解ける.

1. $B(2, n)$ から成る部分系列を元とした集合 $X$ と, 「系列全体からみて, その部分系列を得るにいくつスライドしたか」を元とする集合 $Y$ の写像 $f$ を定める(例: $f(000_{(2)}) = 0, f(001_{(2)}) = 1, f(011_{(2)}) = 2, \cdots f(100_{(2)}) = 7$).
2. $x$ のうち一番右端に立っているビットのみを残し, 他を全て倒す(`x & -x`). この値は必ず $2^i$ である. これを $y$ とする.
3. $B(2, n)$ と $y$ の積を得る($y = 2^i$ であるから, この演算は系列に対する $i$ ビットの左シフト演算である). これを $z_0$ とする.
4. いま, ここまでの演算を $s\ (\{s \in \mathbb{Z}\mid s > n, s は 2 の累乗数\})$ ビットの領域上で行なったとしたとき, $z_0$ に対して $s-n$ ビット左にシフトする($z_0$ を msb から数えて $n$ ビット分のみが必要であるから, それ以外を除去する). これを $z_1$ とする.
5. $f(z_1)$ の値が ntz の解である.

先に述べたとおり, $8$ ビットの領域に対して作用させる De Bruijn Sequence は $B(2, 3) = 11101_{(2)} = 1d_{(16)}$ である.
よって $8$ ビットの領域に対する ntz は, いま述べた手順に従い次のように実装することができる.

```haskell
module Main where

import Data.Array (Array, listArray, (!))
import Data.Tuple.Extra (dupe, first)
import Data.Bits ((.&.), shiftR)
import Data.Word (Word8)
import Test.HUnit (runTestText, putTextToHandle, Test (TestList), (~:), (~?=))
import System.IO (stderr)
import Control.Monad (void)

-- | ntz 8 bit version 2
ntz82 :: Word8 -> Word8
ntz82 = (tb !) . (`shiftR` 4) . fromIntegral . ((0x1d :: Word8) *) . 
    uncurry (.&.) . first negate . dupe
    where
        tb = listArray (0, 14) [
            8, 0, 0, 1, 6, 0, 0, 2, 7, 0, 5, 0, 0, 4, 3] :: Array Int Word8

main :: IO ()
main = void . runTestText (putTextToHandle stderr False) $ 
    TestList ["192 ntz: " ~: ntz82 (192 :: Word8) ~?= 6] 
```

先の`ntz81`の実装と比較して, 全体の演算回数が減っていることは明らかである.

ここまで, $x$ が $8$ ビットである前提を置いて論理展開を行なってきたが, 任意の $B(2, n)$ が求まれば, どのようなビット長のデータに対しても同様にして計算できることがわかる.
これをどのように得るかであるが, ここでは Prefer One algorithm[^3] という比較的単純なアルゴリズムを用いて $B(2, n)$ を得ることとする. 
このアルゴリズムは, 任意の正整数 $n\geq 1$ について, まず $n$ 個の $0$ を置く. 次に, 最後の $n$ ビットによって形成された部分系列が以前に系列内で生成されていなかった場合, その次のビットに対して $1$ を,
そうでない場合 $0$ を置く. $0$ または $1$ のどちらを置いても, 以前に生成していた部分系列と一致するならば停止する.
下記に示す同アルゴリズムの実装例は, $2^3$ から $2^6$ までの De Bruijn Sequence を得ている.

```haskell
module Main where
import Data.Array (listArray, (!), elems)
import Data.Tuple.Extra (dupe, first)
import Control.Monad (mapM_)
import Numeric (showHex)

preferOne :: Int -> [Int]
preferOne n = let s = 2 ^ n in
    let ar = listArray (1, s) $
            replicate n False ++ map (not . yet) [n + 1 .. s]
        yet i = or [map (ar !) [i - n + 1 .. i - 1] ++ [True] == 
            map (ar !) [i1 .. i1 + n - 1] | i1 <- [1 .. i - n]]
    in cycle $ map fromEnum $ elems ar
 
bin2dec :: [Int] -> Int
bin2dec = sum . uncurry (zipWith (*)) . first (map (2^) . takeWhile (>=0) . 
    iterate (subtract 1) . subtract 1 . length) . dupe

somebases :: Int -> IO ()
somebases n = let d = take (2^n) $ L.preferOne n in
    print (d, bin2dec d, showHex (bin2dec d) "")

main :: IO ()
main = mapM_ somebases [3..6]
```

なお, 本章にて取り扱ったソースコードは, 次のリポジトリ [5161001/5161src](https://bitbucket.org/r0ki/5161001/src/master/5161src/) にもアップロードしてある.

また, C++ 言語ではその言語機能により, ビット長に依存せずに同じ関数呼び出しから同様の処理が実行できる[^4]. 
この実装に関しては, 私が開発している C++ ライブラリの一部である, srook/bit/algorithm/ntz.hpp[^5] を参照.

\newpage

# 離散対数問題とエルガマル暗号

離散対数問題によって暗号学的安全性を担保するエルガマル暗号について学ぶことは,
個人的に大変興味深いものであったので, これも私の数学探訪として書く.
本題に入る前に, 必要となる数論的関数の定義および定理の証明を行う.

## ユークリッドの互除法

**ユークリッドの互除法**: $2$ つの自然数 $a, b\in\mathbb{N}$ の最大公約数を求めるアルゴリズム.

最大公約数を求める方法として, 素因数分解をひたすら行うのには, 計算量的に限界がある.
そこで, 古代ギリシャの数学者ユークリッドは, 
この問題を幾何学的に考察した.
たとえば $a=12345678,\ b=87654321$
の最大公約数を求めるとする(以下これを $\gcd(a,b)=c$ と書く).
これをユークリッドの互除法は,
$$
\begin{array}{rcrcr}
87654321&=&12345678\cdot 7&+&1234575\\
12345678&=&1234575\cdot 9&+&1234503\\
1234575&=&1234503&+&72\\
1234503&=&72\cdot 17145&+&63\\
72&=&63&+&9\\
63&=&\underbrace{9}_{c}\cdot 7
\end{array}
$$
より $\gcd(a,b)=9$ というように解く. これで最大公約数を求まる根拠を以下証明する.


**補題 1**: $\gcd(a,b)=\gcd(a-b, b)=\gcd(a-2b,b)=\gcd(a-3b,b)=\cdots\ (a, b\in\mathbb{N})$ が成り立つ.<br>
**証明**: $a,\ b$ の公約数を $d$ とすると, 
$d\mid a\land d\mid b \Rightarrow d\mid a-b$.
また $a-b$ と $b$ の公約数を $e$ とすると, $e\mid a-b\land e\mid b\Rightarrow e\mid (a-b)+b=e\mid a$.

$\therefore$ 公約数の全体が一致するから, 最大公約数も一致して, 
$\gcd(a,b)=\gcd(a-b,b)$. これを繰り返すと $$
\gcd(a,b)=\gcd(a-b,b)=\gcd(a-2b,b)=\gcd(a-3b,b)=\cdots
$$ $\square$

**命題 1**: ユークリッドの互除法により $c$ が最大公約数となる.<br>

**証明**: $a, b\in\mathbb{Z}^{+}$ があるとき, 除算は
$a=bq+r,\ 0\leq r< b$ と表せる.
補題 1 より, $\gcd(a,b)=\gcd(a-bq,b)=\gcd(b,r)$ がいえる.
ここで,

$$
\begin{array}{lclllcl}
a&=&bq_1+r_1& (0< r_1< b),& \gcd(a,b)&=&\gcd(b,r_1)\\
b&=&r_1q_2+r_2& (0< r_2< r_1),& \gcd(b,r_1)&=&\gcd(r_1,r_2)\\
r_1&=&r_2q_3+r_3& (0< r_3< r_2),& \gcd(r_1,r_2)&=&\gcd(r_2,r_3)\\
\cdots &&& \cdots &&& \cdots \\
r_i&=&r_{i+1}q_{i+2}+r_{i+2}& (0< r_{i+2}< r_{i+1}),& \gcd(r_i,r_{i+1})&=&\gcd(r_{i+1},r_{i+2})\\
\cdots &&& \cdots &&& \cdots \\
r_{n-2}&=&r_{n-1}q_n+r_n&(0< r_n< r_{n-1}),&\gcd(r_{n-2},r_{n-1})&=&\gcd(r_{n-1},r_n)\\
r_{n-1}&=&r_nq_{n+1}&& \gcd(r_{n-1},r_n)&=&r_n
\end{array}
$$

として, $(n+1)$ 回で割り切れたとすると, $r_n$ が最大公約数 $c$ となる. $\square$

\newpage

## 拡張ユークリッドの互除法

**拡張ユークリッドの互除法**: ユークリッドの互除法で求まる $\gcd(a,b)$ に加え, $$
ax+by=\gcd(a,b)
$$ (ベズーの等式) が成り立つ $a,\ b$ のベズー係数 $x,\ y$ をも同時に求める.

$a=n=10,\ b=m=97$ を入力として, まず一般解を導いてみる.
いま $n,\ m$ に対してユークリッドの互除法を行うと,

$$
\begin{array}{lcllclcl}
97&=&10\cdot 9+7&\Leftrightarrow &7&=&97-10\cdot 9\\
10&=&7+3&\Leftrightarrow &3&=&10-7\\
7&=&3\cdot 2+1&\Leftrightarrow &1&=&7-3\cdot 2
\end{array}
$$

より $$10x+97(-y)=1$$ と表せる.
これは $1$ 次不定方程式で, $\gcd(a,b)=d$ としたとき $d=1$ ならば解がある.
また, $d\gt 1,\ d\mid c$ でも解がある. 
なぜならば, $d=\gcd(a,b)$ のときはユークリッドの互除法で解が構成できたし,
$d\gt 1,\ d\mid c$ ならば $d$ 倍してやれば良いからである.
逆に $d\not\mid c$ ならば, その $1$ 次不定方程式は不能となる.
いま述べた例の場合, 解は存在して,

$$
\begin{array}{lclclcl}
1&=&7-3\cdot 2\\
1&=&7-(10-7)\cdot 2&=&7\cdot3 - 10\cdot 2\\
1&=&(97-10\cdot 9)\cdot 3-10\cdot 2&=&97\cdot 3-10\cdot 29 
\end{array}
$$

よって, 特別解 $x=-29,\ y=-3$ が求まった. 
次に, この一般解を求める. いま求めた $x,\ y$ を代入すると $10(-29)+97(3)=1$.
これを元の式から引くと,

$$
\begin{array}{rrcrr}
& 10x &+ &97(-y) &= 1 \\
-)&10(-29) &+ &97(3) &= 1\\
\hline
&10(x+29)&+&97(-y-3)&=0
\end{array}
$$

で, 変形すると $10(x+29)=-97(-y-3)$ と表せる. 
ここで, 先のユークリッドの互除法により $\gcd(10,97)=1$ であることがわかっているから, 
$x+29=97n\ (n\in\mathbb{Z})$ と表すことができることがわかる.
よって, $x=97n-29,\ y=10n-3$ である. 

ここで, いまやった一連の作業を一般化しておく. この計算が $(s+1)$ 回で終わったとする.

$$
\begin{array}{llcl}
&r_1&=&a-bq_1\\
&r_2&=&b-r_1q_2\\
&r_3&=&r_1-r_2q_3\\
&\cdots & & \cdots \\
&r_i&=&r_{i-2}-r_{i-1}q_i\\
&\cdots & & \cdots \\
&r_{s-1}&=&r_{s-3}-r_{s-2}q_{s-1}\\
&d&=&r_{s-2}-r_{s-1}q_s
\end{array}
$$

よって $r_i=x_ia+y_ib$ として $x_s,\ y_s$ を求めればよいこととなる.
まず, $r_i=a-bq_i$ から $x_1=1,\ y_1=-q_1$ について代入し 
$$r_2=b-(a-bq_1)q_2=-aq_2+b(1+q_1q_2)$$ とする.
ここで $x_2=-q_2,\ y_2=1+q_1 q_2$ を初期条件とする.
一般に $r_i$ が $a,\ b$ で表せるとしたとき $r_i=x_i a+y_i b$ と表せるから
$x_i,\ y_i$ の漸化式を次のようにおくことができる.

$$
\begin{array}{lcl}
r_{i-2}&=&x_{i-2}a+y_{i-2}b \\
r_{i-1}&=&x_{i-1}a+y_{i-1}b \\
r_i&=&x_i a+y_i b
\end{array}
$$

これらを代入すると,
$$x_i a+y_i b=x_{i-2}a+y_{i-2}b-(x_{i-1}a+y_{i-1}b)q_i$$
両辺の $a,\ b$ の係数を比較すると,

$$
\begin{array}{lcl}
x_i&=&x_{i-2}-x_{i-1}q_i\ \left(i\geq 3\right) \\
y_{i}&=&y_{i-2} - y_{i-1}q_i
\end{array}
$$

よって, いまのように順に $x_3,\ x_4,\ \cdots,\ y_3,\ y_4,\cdots$ と計算していけば
$d$ の表示式である $d=xa+yb$ の $x$ と $y$ が求まる.

\newpage

## オイラーの $\phi$ 関数

オイラーの $\phi$ 関数は, 正整数 $n$ に対する $1$ から $n$ までの自然数のうち
$n$ と互いに素なものの個数を $\phi(n)$ として与えることによって定まる乗法的関数で, 
$p_i$ を $n$ の素因数として, 次の式で定義できる.

$$\phi(n)=n\displaystyle\prod_{i=1}^k(1-\dfrac{1}{p_i})$$

例えば $\phi(14) = 6$ である($14 = 2 \cdot 7$ だから, $14\left(1-\dfrac{1}{2}\right)\left(1-\dfrac{1}{7}\right) = 6$. これを列挙すると, $1,3,5,9,11,13$). 
特に, $n$ が素数で $p$ ある場合, $1$ から $p-1$ のうち $p$ の素因数である 
$p$ を因数としてもつことはないから $\phi(p) = p - 1$ が成り立つ.

## ラグランジュの定理

フェルマーの小定理の証明にラグランジュの定理を使うと,
証明の簡素化のほか, 他の関連定理の証明がついでに行えるので, まずはこれを証明する.

**ラグランジュの定理**:
有限郡 $G$ の部分郡 $H$ の位数 $\mid H\mid$ は, $G$ の位数 $\mid G\mid$ の約数となる.
$$\mid G\mid\ =\ \mid G:H\mid \mid H\mid$$

**証明**: 有限郡 $G$ の部分郡 $H$ による類別が $\displaystyle G=\bigcup_i^r a_iH$ であるとき, $\mid G\mid=r\mid H\mid$ といえる. この $r$ は $r=\mid G:H\mid$ 
そのものなので, $\mid G\mid\ =\ \mid G:H\mid\mid H\mid$. $\square$

\newpage

## オイラーの定理, フェルマーの小定理

ラグランジュの定理を用いてオイラーの定理を証明し, フェルマーの小定理を導く.

**補題2**: 有限郡 $G$ とその元 $^\forall g\in G$ に対し, $g^{\mid G\mid}=e$. $e\in G$ は単位元. 

**証明**:
巡回部分郡 $H=<g>$ の元 $g$ の位数 $\mid H\mid$ は,
巡回して $g^i=e$ となる最小の $i\in\mathbb{N}$ であるといえる. すなわち
$$g^{\mid H\mid}=e$$
ここで, 商集合の位数を両辺に次のように与える.
$$\left(g^{\mid H\mid}\right)^{\mid G:H\mid}=(e)^{\mid G:H\mid}$$
左辺は指数法則により, また右辺は単位元の繰り返しだから, これを次のようにかける.
$$g^{\mid H\mid\mid G:H\mid}=e$$
ラグランジュの定理より
$$g^{\mid G\mid}=e$$
$\square$

**オイラーの定理の証明**:
郡 $G$ とその部分郡 $H$ があるとき, $H$ は郡であるから単位元 $e\in H$ を含む. 
よって, $^\exists a\in G$ の剰余類を $aH=\left\{ah\mid h\in H\right\}$ としたとき(簡単のため, 左剰余類として式をおいたが, これに深い意味はない.), $a=ae\in aH$ より $a\in aH$ である.
オイラーの定理

$$a^{\phi(n)}\equiv 1\pmod{n}\ \left(2\leq n\in\mathbb{Z}^{+},\ \gcd(a,n)=1\right)$$

を仮定したとき, 剰余類 $\overline{a}$ は法 $n$ に関する既約剰余類郡 $\mathbb{Z}^{\ast}_{n}$ に含まれる.  $\mid\mathbb{Z}^{\ast}_{n}\mid=\phi(n)$ だから補題2より 
$\overline{a}^{\phi(n)}=\overline{a^{\phi(n)}}=\overline{1}$. $\square$

**フェルマーの小定理の証明**:
オイラーの定理より, 
$n$ が素数 $p$ であるとき, $\phi(p) = p-1$ よりフェルマーの小定理となる. $\square$

フェルマーの小定理の対偶を利用して素数判定を行う方法が, フェルマーテストである.

\newpage

## 離散対数問題
$g^{f}\equiv n\pmod{p}\ \left(1\leq n \leq p-1\right)$ を満たす
$f$ は $0\leq f\leq p-2$ のうち, ただ $1$ つだけ存在する. これを $n$ の指数または離散対数といい, 
$f=\log_{g}n\pmod{p}$ および $f=Ind_{g}(n)$ と書く. 
この $n$ が真数, または離散真数である.
以下は, $3\leq p \leq 19\ \left(p\ is\ prime\right)$ でその最小の原始根 $g$ の冪乗
$n\equiv g^f\pmod{p}$ の昇順を $x$ 軸, 
$f=Ind_{g}(n)$ を $y$ 軸として,
それぞれの各離散対数をプロットした図である[^6].

![離散対数を視覚化した図](./assets/fig.png)

これを見てもわかるように, $f$ の値に規則性は見られず, 予測困難な振る舞いをすることがわかる.
例えば, $p=19,\ g=2,\ n=3$ とすると
```haskell
Prelude> head [f | f <- [0..17], 2^f `mod` 19 == 3 `mod` 19]
13
```
より $f=13$ であることがわかる. この場合, まだ $p$ が小さい素数であるからこそ, 
このような総当たりで解が得られるが,
大きな $p$ に対する総当たりでは, 実用的な時間で解を得ることができない.
エルガマル暗号は, $g$ と $f$ から $n$ を求めることは容易であるが,
いま述べたように $g$ と $n$ から $f$ を求めることは困難であるという事実を利用することで,
公開鍵暗号方式としての成立および暗号学的安全性の担保を確立する.


## 暗号の生成と解読

暗号の生成とその解読方法について示す.
受信者は下準備として次の手順で公開鍵と秘密鍵を生成する:

1. 大きな素数 $p$ (とくに安全素数であることが多い)をフェルマーテスト, ミラーラビン素数判定法などで選ぶ.
2. $\phi(p-1)$ 個の $p$ の原始根のうち, 任意の $1$ つ $g$ を選ぶ.
3. 任意の正整数 $a\in\mathbb{Z}^{+},\ \left(a < p\right)$ を選ぶ.
4. $y\equiv g^a\pmod{p}$ を計算する.
5. 公開鍵を $\left\{p, g, y\right\}$, 秘密鍵を $\left\{a\right\}$ とする.

平文の列を $x_1,x_2,\cdots,x_t\ \left(x_i < p\right)$, 最初の平文を $x$ とし,
発信者は次の手順で暗号文を生成する:

1. 任意の正整数 $k\in\mathbb{Z}^{+}$ を選ぶ.
2. $\alpha\equiv g^k\pmod{p}$ を計算する.
3. $z\equiv y^k\pmod{p}$ と $\beta\equiv xz\pmod{p}$ を計算し, $x$ に対する $\gamma =\left\{\alpha,\ \beta\right\}$ を得る.
4. $x_t$ に到達するまで 1 から 3 の手順を繰り返す. 到達すれば, 暗号文の生成は完了である.

受信者は次の手順で暗号を解く:

1. $\gamma$ から $x$ を得るためには, $y^k$ を要する. 秘密鍵 $a$ を使い, $z\equiv y^k \equiv \left(g^a\right)^k\equiv\left(g^k\right)^a\equiv\alpha^a\pmod{p}$ と計算する.
2. $x\equiv \dfrac{\beta}{z}\pmod{p}$ であるので, $\pmod{p}$ で $z$ のモジュラ逆数 $zq\equiv 1\pmod{p},\ q\equiv\dfrac{1}{z}\pmod{p}$ を計算する. このときの逆数は, 拡張ユークリッドの互除法によって求める.
3. $x\equiv\dfrac{\beta}{z}\equiv\beta q\equiv u\pmod{p}\ \left(u< p\right)$ を計算する. ここで, $x< p$ であるから, この結果が平文である.

実際にこれを手計算で実行してみる.
$p=97$ とする. 従って $g=5$ となる. ここで $a=7$ とする.
$y\equiv 5^7\pmod{97}$ より $y\equiv 5^7\equiv 40\pmod{97}$ だから, 公開鍵は $\left\{97,5,40\right\}$,
秘密鍵は $\left\{7\right\}$ である. 次に暗号文を作成する.
平文は, 次のアスキーコードで表現された文字列とする.

```haskell
Prelude> :m +Data.Char
Prelude Data.Char> map ord "OK"
[79,95]
```

ここで $x=79,\ k = 5$ とする. 
$\alpha\equiv 5^5\equiv 21\pmod{97}$ また
$z\equiv 40^5\equiv 10\pmod{97}$ より $\beta\equiv 79\cdot 10\equiv 14\pmod{97}$.
従って, $\gamma=\left\{21, 14\right\}$ となる. 
$x=95$ にも同様の計算($k$ は毎度ランダムに選ぶ. 次は $k=6$ であったとした.)を施して, 
全体の暗号文を`[21, 14, 8, 73]`とする. これを解読する.
$z\equiv 21^7\equiv 10\pmod{97}$ で, $z\equiv y^k\pmod{97}$.
$10q\equiv 1\pmod{97}$ だから $q\equiv\dfrac{1}{10}\equiv 68\pmod{97}$.
ここで $u\equiv 14\cdot 68\equiv 97\pmod{p}$ で, $u< p$ だから $x\equiv u\pmod{p}$.
よって $1$ 文字目は $79$. 同様に $2$ 文字目も計算し, 全体の平文が手に入る.
解読の段階で $a=7$ を知らなかった場合, 離散対数問題を解くことに相当するため, 
平文を得るのは非常に困難となる.

## 実装

エルガマル暗号へのエンコーダ, およびデコーダを Haskell で実装した.

* [falgon/ElgamalEncryptionHs: The rustic implementation of ElGamal encryption encoder and its decoder.](https://github.com/falgon/ElgamalEncryptionHs)

なお実装内では, 本レポート内で詳細に触れていない
いくつかの工夫(ミラーラビン素数判定法, 安全素数, witness マジックナンバー等)が用いられている.
それらに関する言及は, [私の個人ブログ内のエントリ](https://falgon.github.io/roki.log/posts/2018/%207/13/elgamalEncryption/)を参照.

\newpage

# 参考文献

* 芹沢正三 (2002) 『素数入門―計算しながら理解できる』 ブルーバックス. ISBN-10: 4062573865
* 雪江明彦 (2010) 『代数学1　群論入門 (代数学シリーズ)』 日本評論社. ISBN-10: 4535786593
* <a name="ref3" style="pointer-events: none; color: #000;">von zur Gathen, Joachim; Shparlinski, Igor (1998), “Orders of Gauss periods in finite fields”, Applicable Algebra in Engineering, Communication and Computing</a>
* <a name="ref4" style="pointer-events: none; color: #000;">Robbins, Neville (2006), Beginning Number Theory, Jones & Bartlett Learning, ISBN 978-0-7637-3768-9.</a>

\newpage

# 謝辞

コンピュータ数学ならびに数学探訪の講義の受講により, 
数学的分野への興味, 関心がより一層深まりました.
また終始私の質問や疑問に熱心にご対応頂き, 大変お世話になりました.
この度はありがとうございました.

[^2]: Python, networkx, pyplot で[生成](https://gist.github.com/falgon/a3da8e0fd013f41de62e0d7a0288a66d).
[^3]: Abbas Alhakim, "A SIMPLE COMBINATORIAL ALGORITHM FOR DE BRUIJN SEQUENCES" <https://www.mimuw.edu.pl/~rytter/TEACHING/TEKSTY/PreferOpposite.pdf> 2018-06-21 アクセス.
[^4]: haskell でもデータ型と値コンストラクタを使って似たようなことが可能であるが, 新たに型を定義しなければならない点を考慮すると, C++ で実装した方がより汎用性が高いといえる.
[^5]: Github: <https://github.com/falgon/SrookCppLibraries/blob/acfad043881d5559f921d547331ba9d5ec1b1d9f/srook/bit/algorithm/ntz.hpp>
[^6]: Python, matplotlib で[生成](https://gist.github.com/falgon/97dc3c1a399422a3ab2818d01b70a14b).
