import qualified Lib as L
import Ntz
import Test.HUnit (runTestText, putTextToHandle, Test (TestList), (~:), (~?=))
import System.IO (stderr)
import Control.Monad (void)
import Data.Word (Word8)

main :: IO ()
main = void . runTestText (putTextToHandle stderr False) $ 
    TestList [
        "0xc0 ntz81: " ~: L.ntz81 (0xc0 :: Word8) ~?= 6,
        "0xc0 ntz82: " ~: L.ntz82 (0xc0 :: Word8) ~?= 6,
        "0x80 ntz81: " ~: L.ntz81 (0x80 :: Word8) ~?= 7,
        "0x80 ntz82: " ~: L.ntz82 (0x80 :: Word8) ~?= 7,
        "0x20 ntz81: " ~: L.ntz81 (0x20 :: Word8) ~?= 5,
        "0x20 ntz82: " ~: L.ntz82 (0x20 :: Word8) ~?= 5,
        "0x10 ntz81: " ~: L.ntz81 (0x10 :: Word8) ~?= 4,
        "0x10 ntz82: " ~: L.ntz82 (0x10 :: Word8) ~?= 4,
        "0x8 ntz81: " ~: L.ntz81 (0x8 :: Word8) ~?= 3,
        "0x8 ntz82: " ~: L.ntz82 (0x8 :: Word8) ~?= 3,
        "0x4 ntz81: " ~: L.ntz81 (0x4 :: Word8) ~?= 2,
        "0x4 ntz82: " ~: L.ntz82 (0x4 :: Word8) ~?= 2,
        "0x2 ntz81: " ~: L.ntz81 (0x2 :: Word8) ~?= 1,
        "0x2 ntz82: " ~: L.ntz82 (0x2 :: Word8) ~?= 1,
        "0 ntz81: " ~: L.ntz81 (0 :: Word8) ~?= 8,
        "0 ntz82: " ~: L.ntz82 (0 :: Word8) ~?= 8,
        "8 bit 0xc0 ntz: " ~: ntz (Bit8 0xc0) ~?= 6,
        "16 bit 0xc0 ntz: " ~: ntz (Bit16 0xc0) ~?= 6,
        "32 bit 0xc0 ntz: " ~: ntz (Bit32 0xc0) ~?= 6,
        "64 bit 0xc0 ntz: " ~: ntz (Bit64 0xc0) ~?= 6]
