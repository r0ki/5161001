{-# OPTIONS_GHC -Wall -Werror #-}

module Lib (
    preferOne,
    ntz81,
    ntz82
) where

import Data.Array (Array, listArray, (!), elems)
import Data.Tuple.Extra (dupe, first, second)
import Data.Bits ((.&.), shiftR)
import Data.Word (Word8)
import Data.Bool (bool)

type BitSeq = [Int]

-- | Constructing B(2, n) De Bruijn Sequence by 
-- preferOne(https://www.mimuw.edu.pl/~rytter/TEACHING/TEKSTY/PreferOpposite.pdf) algorithm.
preferOne :: Int -> BitSeq
preferOne n = let s = 2 ^ n in
    let ar = listArray (1, s) $ replicate n False ++ map (not . yet) [n + 1 .. s]
        yet i = or [map (ar !) [i - n + 1 .. i - 1] ++ [True] == map (ar !) [i1 .. i1 + n - 1] | i1 <- [1 .. i - n]] in 
            cycle $ map fromEnum $ elems ar

-- | pop counting 8 bit
popcnt8 :: Word8 -> Word8
popcnt8 = let fol m s = uncurry (+) . first (.&. m) . second ((.&. m) . (`shiftR` s)) . dupe in
    flip (foldr id) [fol (0x0f :: Word8) 4, fol (0x33 :: Word8) 2, fol (0x55 :: Word8) 1]
 
 -- | ntz 8 bit version 1
ntz81 :: Word8 -> Word8
ntz81 = uncurry id . first (bool (popcnt8 . pred . uncurry (.&.) . second negate . dupe) (\_ -> 8 :: Word8) . (== 0)) . dupe

-- | ntz 8 bit version 2
ntz82 :: Word8 -> Word8
ntz82 = (tb !) . (`shiftR` 4) . fromIntegral . ((0x1d :: Word8) *) . uncurry (.&.) . first negate . dupe
    where
        tb = listArray (0, 14) [8, 0, 0, 1, 6, 0, 0, 2, 7, 0, 5, 0, 0, 4, 3] :: Array Int Word8
