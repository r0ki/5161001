{-# OPTIONS_GHC -Wall -Werror #-}

module Ntz (
    BitData (..),
    ntz
) where

import Data.Array (Array, listArray, (!))
import Data.Tuple.Extra (dupe, first)
import Data.Bits ((.&.), shiftR)
import Data.Word

data BitData = Bit8 Word8 | Bit16 Word16 | Bit32 Word32 | Bit64 Word64 deriving (Eq, Ord, Show)

ntz :: BitData -> Int
ntz (Bit8 x) = ((tb !) . fromIntegral . (`shiftR` 4) . ((0x1d :: Word8) *) . uncurry (.&.) . first negate . dupe) x
    where   
        tb = listArray (0, 14) [8, 0, 0, 1, 6, 0, 0, 2, 7, 0, 5, 0, 0, 4, 3] :: Array Int Int

ntz (Bit16 x) = ((tb !) . fromIntegral . (`shiftR` 11) . ((0x0f2d :: Word16) *) . uncurry (.&.) . first negate . dupe) x
    where
        tb = listArray (0, 31) [16, 0, 0, 1, 0, 8, 0, 2, 14, 0, 0, 9, 0, 11, 0, 3,
                                15, 0, 7, 0, 13, 0, 10, 0, 0, 6, 12, 0, 5, 0, 4] :: Array Int Int

ntz (Bit32 x) = ((tb !) . fromIntegral . (`shiftR` 26) . ((0x07c56e99 :: Word32) *) . uncurry (.&.) . first negate . dupe) x
    where
        tb = listArray (0, 63) [32, 0, 0, 1, 0, 10, 0, 2, 29, 0, 11, 0, 25, 0, 0, 3,
                                30, 0, 0, 23, 0, 12, 14, 0, 0, 26, 0, 16, 0, 19, 0, 4,
                                31, 0, 9, 0, 28, 0, 24, 0, 0, 22, 0, 13, 0, 15, 18, 0,
                                0, 8, 27, 0, 21, 0, 0, 17, 7, 0, 20, 0, 6, 0, 5] :: Array Int Int

ntz (Bit64 x) = ((tb !) . fromIntegral . (`shiftR` 57) . ((0x03f0a933adcbd8d1 :: Word64) *) . uncurry (.&.) . first negate . dupe) x
    where
        tb = listArray (0, 127) [64,  0, 0,  1, 0, 12, 0,  2, 60, 0, 13, 0, 0, 53, 0,  3,
                                 61, 0, 0, 21, 0, 14, 0, 42, 0, 24, 54, 0, 0, 28, 0,  4,
                                 62, 0, 58, 0, 19, 0, 22, 0, 0, 17, 15, 0, 0, 33, 0, 43,
                                 0, 50, 0, 25, 55, 0, 0, 35, 0, 38, 29, 0, 0, 45, 0,  5,
                                 63, 0, 11, 0, 59, 0, 52, 0, 0, 20, 0, 41, 23, 0, 27, 0,
                                 0, 57, 18, 0, 16, 0, 32, 0, 49, 0, 0, 34, 37, 0, 44, 0,
                                 0, 10, 0, 51, 0, 40, 0, 26, 56, 0, 0, 31, 48, 0, 36, 0,
                                 9, 0, 39, 0, 0, 30, 47, 0,  8, 0, 0, 46,  7, 0,  6] :: Array Int Int
