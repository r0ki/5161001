# 5161src

DHU 講義 51610001 レポート内で示したコード.
```sh
$ stack build && stack exec 5151src-exe
```
によって B(2, 3) から B(2, 6) の系列を標準出力に出力する.

```sh
$ stack test
```
によって ntz のテストを実行する.
